<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\producto; 
class Producto extends Model
{
    
        protected $fillable =[
           'nombre',
           'tipo',
           'estado',
           'precio',
        ];
    
}
