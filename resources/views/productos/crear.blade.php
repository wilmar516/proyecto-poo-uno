@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR UN PRODUCTO</div>
                <div class="col text-right">
                    <a href="{{ route('lista.productos') }}" class="btn btn-sm btn-success">Cancelar</a>
                    </div> 


                <div class="card-body">
                <form role="form" method="POST" action="{{ route('guardar.productos')}}">
                    {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="row">
                            <div class="col-lg-4">
                            <label class="from-control-label" for="nombre">Nombre del producto</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>
                        <div class="col-lg-4">
                            <label class="from-control-label" for="tipo">Tipo producto</label>
                            <input type="text" class="from-control" name="tipo">
                        </div>
                        <div class="col-lg-4">
                            <label class="from-control-label" for="estado">Estado producto</label>
                            <input type="text" class="from-control" name="estado">
                        </div>
                        <div class="col-lg-4">
                            <label class="from-control-label" for="precio">Precio</label>
                            <input type="text" class="from-control" name="precio">
                            <button type="submit" class="btn btn-xs btn-success pull-right">Guardar</buttton>

                        </div>

                    </form>
                    


