@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
               
                    @endif
                    <div class="card-deck">
                <div class="card">
                    <a href="{{ route('lista.productos') }}" class="btn btn-sm btn-success">Productos</a>
                    </div>
                </div>
                <div class="card">
                <div class="card text-center">
                    <a href="{{ route('listar.clientes') }}" class="btn btn-sm btn-success">Clientes</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
