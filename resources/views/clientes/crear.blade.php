@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR UN CLIENTE</div>
                <div class="col text-right">
                    <a href="{{ route('listar.clientes') }}" class="btn btn-sm btn-success">Cancelar</a>
                    </div> 


                <div class="card-body">
                <form role="form" method="POST" action="{{ route('guardar.clientes')}}">
                    {{ csrf_field() }}
                        {{ method_field('post') }}

                        <div class="row">
                            <div class="col-lg-4">
                            <label class="from-control-label" for="nombre">Nombre del cliente</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-label" for="apellido">Apellido Cliente</label>
                            <input type="text" class="from-control" name="apellido">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-label" for="cedula">Cedula</label>
                            <input type="text" class="from-control" name="cedula">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-label" for="direccion">Direccion</label>
                            <input type="text" class="from-control" name="direccion">
                        </div> 

                        <div class="col-lg-4">
                            <label class="from-control-label" for="telefono">Telefono</label>
                            <input type="text" class="from-control" name="telefono">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-label" for="nacimiento">Fecha de nacimiento</label>
                            <input type="text" class="from-control" name="nacimiento">
                        </div>

                         <div class="col-lg-4">
                            <label class="from-control-label" for="email">email</label>
                            <input type="text" class="from-control" name="email">
                        </div>
                        </div>
                            <button type="submit" class="btn btn-sm btn-success pull-right">Guardar</buttton>
                            
                        
                        </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection