<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioCliente(Request $request)
    {
     $Cliente = Cliente::all();  
     return view('clientes.inicio')->with('Cliente', $Cliente);

    }

    public function CrearCliente(Request $request)
    {
     $Cliente = Cliente::all();  
     return view('clientes.crear')->with('Cliente', $Cliente);

    }

    public function GuardarCliente(Request $request)
    {
    $this->validate($request,[
        'nombre' => 'required',
        'apellido' => 'required',
        'cedula' => 'required',
        'direccion' => 'required',
        'telefono' => 'required',
        'nacimiento' => 'required',
        'email' => 'required',
    ]);

    $cliente = new Cliente;
    $cliente->nombre  = $request->nombre;
    $cliente->apellido= $request->apellido;
    $cliente->cedula  = $request->cedula;
    $cliente->direccion = $request->direccion;
    $cliente->telefono = $request->telefono;
    $cliente->nacimiento  = $request->nacimiento;
    $cliente->email  = $request->email;
    $cliente->save();
    
    return redirect()->route('listar.clientes');
}
}