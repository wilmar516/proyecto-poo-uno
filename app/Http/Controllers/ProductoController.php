<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioProducto(Request $request)
    {
     $Producto = Producto::all();  
     return view('productos.inicio')->with('Producto', $Producto);

    }

    public function CrearProducto(Request $request)
    {
     $Producto = Producto::all();  
     return view('productos.crear')->with('Producto', $Producto);

    }

    public function GuardarProducto(Request $request)
    {
    $this->validate($request,[
        'nombre' => 'required',
        'tipo' => 'required',
        'estado' => 'required',
        'precio' => 'required',
    ]);

    $producto = new Producto;
    $producto->nombre  = $request->nombre;
    $producto->tipo    = $request->tipo;
    $producto->estado  = $request->estado;
    $producto->precio  = $request->precio;   
    $producto->save();
    
    return redirect()->route('lista.productos');
}
}